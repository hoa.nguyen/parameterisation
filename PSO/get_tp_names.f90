program get_tp_names

        implicit none

        integer, parameter :: n_p = 4
        integer :: i

        include 'tuned_p.f90'

        OPEN(UNIT=20,FILE='tuned_p_names.txt', ACTION="write", STATUS="replace")
        do i=1,n_p
           WRITE(20,'(A,A,F5.2,A,F5.2)') tp_names(i),' ',tp_minmax(i,1),' ',tp_minmax(i,2)
        end do
        CLOSE(UNIT=20)

end program
