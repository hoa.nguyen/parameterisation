#!/bin/bash

for var in "$@"
do
	# edit the programm to write parameter set at given iteration ("var") to yaml file
        # 26: the line want to edit - CHANGE IF NEEDED
        sed -i '26s/ .*/     NUM_LINES = '$var'/' exp_it_gbestpos.f90
        # compile the program
        gfortran -o exp_it_gbestpos.o exp_it_gbestpos.f90
        # run the program to update the fabm.yaml file
        ./exp_it_gbestpos.o
        # compile the code to run the bgc model
        gfortran -o call_bgc_model_short.o call_bgc_model_short.f90
        ./call_bgc_model_short.o
        # rename the output to present the given iteration
        # CHANGE FILE NAMES CORRESPONDINGLY TO ITS OWN CASE
        printf -v new_fname 'sylt_it%d.nc' "$((var))" # new file name with given iteration
        mv -f sylt.nc $new_fname #
done




