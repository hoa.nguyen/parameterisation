!common /t_params/ tp_names, tp_minmax

      !include 'n_tuned_p.f90'


      character(len=30) :: tp_names(4)
      real :: tp_minmax(4,2) 

!      tp_names(1) = 'g2'
!      tp_minmax(1,:) = [10.0, 30.0]

      tp_names(1) = 'muPl'
      tp_minmax(1,:) = [0.7, 2.6]

      tp_names(2) = 'muPs'
      tp_minmax(2,:) = [0.5, 2.0]

!      tp_names(4) = 'aa'
!      tp_minmax(4,:) = [0.01, 0.06]
!      
!      tp_names(5) = 'EXphy'
!      tp_minmax(5,:) = [0.02, 0.1] ![0.015, 0.06]
!
!      tp_names(6) = 'EXdet'
!      tp_minmax(6,:) = [0.015, 0.06]
!
!      tp_names(7) = 'EXdom'
!      tp_minmax(7,:) = [0.03, 0.07] ![0.015, 0.06]
!
!      tp_names(8) = 'rNH4'
!      tp_minmax(8,:) = [0.1, 0.4]
!
!      tp_names(9) = 'rNO3'
!      tp_minmax(9,:) = [0.25, 0.9]
!
!      tp_names(10) = 'psi'
!      tp_minmax(10,:) = [1.5, 6.0]

      tp_names(3) = 'mPl'
      tp_minmax(3,:) = [0.02, 0.08]

      tp_names(4) = 'mPs'
      tp_minmax(4,:) = [0.04, 0.16]

!      tp_names(2) = 'GrZlP'
!      tp_minmax(2,:) = [0.5, 1.8] ![0.4, 1.6]
!
!      tp_names(3) = 'GrZsP'
!      tp_minmax(3,:) = [0.5, 2.0]
!
!      tp_names(4) = 'GrZlZ'
!      tp_minmax(4,:) = [0.25, 1.0]
!
!      tp_names(5) = 'Rg'          ! Zs, Zl half saturation
!      tp_minmax(5,:) = [0.1, 0.8] ![0.25, 1.0]
!
!      tp_names(6) = 'mZl'
!      tp_minmax(6,:) = [0.025, 0.15] ![0.05, 0.2]
!
!      tp_names(7) = 'mZs'
!      tp_minmax(7,:) = [0.05, 0.3] ![0.1, 0.4]
!
!      tp_names(8) = 'excZl'
!      tp_minmax(8,:) = [0.03, 0.12]
!
!      tp_names(9) = 'excZs'
!      tp_minmax(9,:) = [0.04, 0.16]
!
!      tp_names(10) = 'gammaZlp'
!      tp_minmax(10,:) = [0.5, 2.0] ![0.3, 1.5]
!
!      tp_names(11) = 'gammaZsp'
!      tp_minmax(11,:) = [0.5, 2.0] ![0.3, 1.5]
!
!      tp_names(12) = 'gammaZd'
!      tp_minmax(12,:) = [0.5, 2.0] ![0.3, 1.5]

!      tp_names(24) = 'reminD'
!      tp_minmax(24,:) = [0.003, 0.008]
!
!      tp_names(25) = 'sinkDet'
!      tp_minmax(25,:) = [2.5, 10.0]
!
!      tp_names(26) = 'rPO4'
!      tp_minmax(26,:) = [0.025, 0.1]
!
!      tp_names(27) = 'rSi'
!      tp_minmax(27,:) = [0.25, 0.9]
!
!      tp_names(28) = 'regenSi'
!      tp_minmax(28,:) = [0.007, 0.03]
!
!      tp_names(29) = 'crBotStr'
!      tp_minmax(29,:) = [0.003, 0.14]
!
!      tp_names(30) = 'resuspRt'
!      tp_minmax(30,:) = [2.5, 50.0]
!
!      tp_names(31) = 'sedimRt'
!      tp_minmax(31,:) = [3.0, 10.0] ![1.7, 7.0]
!
!      tp_names(32) = 'burialRt'
!      tp_minmax(32,:) = [0.000005, 0.00001]
!
!      tp_names(33) = 'reminSED'
!      tp_minmax(33,:) = [0.0005, 0.002]
!
!      tp_names(34) = 'TctrlDenit'
!      tp_minmax(34,:) = [0.07, 0.3]
!
!      tp_names(35) = 'RelSEDp1'
!      tp_minmax(35,:) = [0.07, 0.3]
!
!      tp_names(36) = 'RelSEDp2'
!      tp_minmax(36,:) = [0.05, 0.2]
!
!      tp_names(37) = 'reminSEDsi'
!      tp_minmax(37,:) = [0.0001, 0.0004]
!
!      tp_names(38) = 'sinkOPAL'
!      tp_minmax(38,:) = [4.0, 12.0] ![2.5, 10.0]

!      tp_names(39) = 'GrF2MB2'
!      tp_minmax(39,:) = [0.001, 0.1]

!      tp_names(13) = 'prefZsPs'
!      tp_minmax(13,:) = [0.35, 1.4]
!
!      tp_names(14) = 'prefZsPl'
!      tp_minmax(14,:) = [0.125, 0.5]
!
!      tp_names(15) = 'prefZsD'
!      tp_minmax(15,:) = [0.001, 0.16]
!
!      tp_names(16) = 'prefZlPs'
!      tp_minmax(16,:) = [0.05, 0.2]
!
!      tp_names(17) = 'prefZlPl'
!      tp_minmax(17,:) = [0.4, 1.5] ![0.4, 1.7]
!
!      tp_names(18) = 'prefZlZs'
!      tp_minmax(18,:) = [0.07, 0.3]
!
!      tp_names(19) = 'prefZlD'
!      tp_minmax(19,:) = [0.1, 0.23] ![0.04, 0.16]

!      tp_names(47) = 'frr'
!      tp_minmax(47,:) = [0.2, 0.9]
!
!      tp_names(48) = 'alfaPs'
!      tp_minmax(48,:) = [0.02, 0.08]
!
!      tp_names(49) = 'alfaPl'
!      tp_minmax(49,:) = [0.026, 0.165]

!      tp_names(46) = 'prefZlZs'
!      tp_minmax(46,:) = [0.075, 0.3]

!      tp_names(47) = 'prefZlD'
!      tp_minmax(47,:) = [0.04, 1.6]

!      tp_names(48) = 'frr'
!      tp_minmax(48,:) = [0.2, 0.8]

!      tp_names(49) = 'MINchl2nPs'
!      tp_minmax(49,:) = [0.25, 1.0]

!      tp_names(50) = 'MAXchl2nPs'
!      tp_minmax(50,:) = [1.915, 7.66]

!      tp_names(51) = 'MINchl2nPl'
!      tp_minmax(51,:) = [0.25, 1.0]

!      tp_names(52) = 'MAXchl2nPl'
!      tp_minmax(52,:) = [1.47, 5.88]

!      tp_names(53) = 'alfaPs'
!      tp_minmax(53,:) = [0.02, 0.08]

!      tp_names(54) = 'alfaPl'
!      tp_minmax(54,:) = [0.026, 0.1]

!      tp_names(55) = 'mMB1'
!      tp_minmax(55,:) = [0.0005, 0.002]

!      tp_names(56) = 'excMB1'
!      tp_minmax(56,:) = [0.0125, 0.05]

!      tp_names(57) = 'rMB1'
!      tp_minmax(57,:) = [0.25, 1.0]

!      tp_names(58) = 'GrMB1Z'
!      tp_minmax(58,:) = [0.05, 0.2]

!      tp_names(59) = 'GrMB1P'
!      tp_minmax(59,:) = [0.05, 0.2]

!      tp_names(60) = 'GrMB1Det'
!      tp_minmax(60,:) = [0.05, 0.2]

!      tp_names(61) = 'GrMB1Sed'
!      tp_minmax(61,:) = [0.05, 0.2]

!      tp_names(62) = 'prefMB1Zs'
!      tp_minmax(62,:) = [0.1, 0.4]

!      tp_names(63) = 'prefMB1Zl'
!      tp_minmax(63,:) = [0.15, 0.6]

!      tp_names(64) = 'prefMB1Det'
!      tp_minmax(64,:) = [0.05, 0.2]

!      tp_names(65) = 'prefMB1Sed'
!      tp_minmax(65,:) = [0.05, 0.2]

!      tp_names(66) = 'prefMB1P'
!      tp_minmax(66,:) = [0.1, 0.4]

!      tp_names(67) = 'prefMB1DOM'
!      tp_minmax(67,:) = [0.05, 0.2]

!      tp_names(68) = 'mMB2'
!      tp_minmax(68,:) = [0.0005, 0.002]

!      tp_names(69) = 'mMB2Predation'
!      tp_minmax(69,:) = [0.0005, 0.002]

!      tp_names(70) = 'excMB2'
!      tp_minmax(70,:) = [0.0175, 0.05]

!      tp_names(71) = 'rMB2'
!      tp_minmax(71,:) = [0.25, 1.0]

!      tp_names(72) = 'gammaMB2'
!      tp_minmax(72,:) = [0.25, 1.0]

!      tp_names(73) = 'GrMB2P'
!      tp_minmax(73,:) = [0.05, 0.2]

!      tp_names(74) = 'GrMB2Z'
!      tp_minmax(74,:) = [0.05, 0.2]

!      tp_names(75) = 'GrMB2Det'
!      tp_minmax(75,:) = [0.05, 0.2]

!      tp_names(76) = 'prefMB2P'
!      tp_minmax(76,:) = [0.1, 0.4]

!      tp_names(77) = 'prefMB2Z'
!      tp_minmax(77,:) = [0.1, 0.4]

!      tp_names(78) = 'prefMB2Det'
!      tp_minmax(78,:) = [0.1, 0.4]

!      tp_names(79) = 'mF1'
!      tp_minmax(79,:) = [0.0005, 0.002]

!      tp_names(80) = 'excF1'
!      tp_minmax(80,:) = [0.01, 0.004]

!      tp_names(81) = 'rF1'
!      tp_minmax(81,:) = [0.35, 1.4]

!      tp_names(82) = 'rF1MB'
!      tp_minmax(82,:) = [0.45, 1.8]

!      tp_names(83) = 'tempcF1'
!      tp_minmax(83,:) = [0.25, 1.0]
