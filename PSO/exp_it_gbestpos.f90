program exp_it_gbestpos
     implicit none
     character(len=30)::fmt11
     integer,parameter::dimen=4,fid=23
     real::gbest_x(dimen), temp(dimen)
     integer::i,tt,NUM_LINES=0,IOS=0


     !include 'tuned_p_names.f90'
!====================
     
     WRITE(fmt11, '(A,I2,A)') '(', dimen, '(1X,F7.3))'
     OPEN(UNIT=fid, FILE='g_bestpos_it.txt',ACTION="read")

     ! count number of lines in the file
     do
        read(unit=fid,fmt=fmt11,iostat=IOS) temp
        if(IOS/=0) exit
        NUM_LINES = NUM_LINES+1
     end do
     !-----


     rewind(UNIT=fid)
     ! UNCOMMENT the line below to write best position at given iteration
     NUM_LINES = 4
     do i=1,NUM_LINES
        read(fid,fmt=fmt11,end=100) temp
        if (i.eq.NUM_LINES) then
        !if (i.eq.NINT(NUM_LINES/2.0)) then ! or take mid value
           gbest_x = temp
           !print *, gbest_x
        end if
     end do

    !---- write to fabm.yaml
       !-- unscaled parameter
       do i =1,dimen
          call sub_edit_fabmyaml(i, gbest_x(i))
       end do
    !-----

!     DO
!        !READ(UNIT=23,FMT='(40(1X,F10.4))',IOSTAT=IOS) temp
!        READ(UNIT=23,FMT=fmt11,IOSTAT=IOS) (temp(tt),tt=1,dimen)
!        IF(IOS.EQ.0) THEN
!                gbest_x = temp
!                print *, gbest_x
!        ELSE
!                EXIT
!        END IF
!
!     END DO
!
100     CLOSE(UNIT=fid)

!===================
     contains
             include 'sub_edit_fabmyaml.f90'
end program
