function fn_cost_func(obs_file, mod_file, mod_varname)

        character(len=*), intent(in) :: obs_file 
        character(len=*), intent(in) :: mod_file 
        character(len=*), intent(in) :: mod_varname 

        !--- variable hold model skill score and to return to the function call
	real :: fn_cost_func
        !------------------------------

        !--- variables to read .nc file of model output
        character(len=100) :: nc_infile
        character(len=32), allocatable :: dimnames(:)
        integer :: var_sz, nc_len
        integer, allocatable :: dimlens(:)
        real, allocatable :: mod_vals(:,:,:,:)
        integer, allocatable :: nct(:)
        !-------------------------------

        !--- declare variables to read .dat file of observation
        integer, parameter :: fid = 111
        character(len=256) :: dt_storage
        character(len=20), allocatable :: obs_date(:)
        integer, allocatable :: obs_datenum(:)
        real, allocatable :: obs_datenum_f(:)
        real(kind=8), allocatable :: obs_vals(:)
        integer :: i = 0, ios=0, NUM_LINES=0
        integer :: j, k
        !---------------------------------------

        !--- declare variables in calculating cost fuction
        integer :: n
        integer, parameter :: c = 2
        real :: Obar, Obar_log
        real :: wss=0.0, wss1=0.0, wss2=0.0, wss1_log, wss2_log
        real, allocatable :: dm1(:), dm2(:)
        integer, allocatable :: dm3(:)
        real, allocatable :: Oi(:), Mi(:), Oi_log(:), Mi_log(:)

        !--- other variables
        integer, allocatable :: obs_dm_t(:), mod_dm_t(:)
        real, allocatable :: obs_dm_val(:), mod_dm_val(:)
        integer :: dm_pointer = 0, new_len = 0, intdm = 0



        !--- READ from .dat file
        !print *, "read in ", mod_varname, " observation data from .dat file"
        open(fid, file=obs_file, status='old', action='read')
        ! count number of lines in the file
        do
                read(fid,*,iostat=ios) dt_storage
                if (ios /=0) exit ! or do while (ios==0) 
                                  ! then need NUM_LINES = NUM_LINES-1 after [end do] 
                NUM_LINES = NUM_LINES + 1
        end do
        !NUM_LINES = NUM_LINES - 1 ! uncomment if use [do while]

        !--- read the file content
        allocate(obs_date(NUM_LINES),obs_datenum(NUM_LINES), obs_datenum_f(NUM_LINES), obs_vals(NUM_LINES))
        rewind(fid) ! move the pointer to the beginning of the file again
        !read(fid,*) ! repeat this to skip the header(s) or not actual data 
        do i = 1,NUM_LINES
                read(fid,*,end=100) obs_date(i), obs_datenum_f(i), obs_vals(i)
                obs_datenum(i) = int(obs_datenum_f(i))
                ! uncomment line 72 if use following lines
                !write(*,10) "line: ", i, " date: ", obs_date(i), &
                !                         " date num: " , obs_datenum(i), &
                !                         " and value:  ", obs_vals(i)
        end do
!10      format(a6,i4,a6,a10,a8,i6,a20,f8.2) ! for printing to check value
100        close(fid)



        !--- READ from .nc file
        !print *, "read model output of ", mod_varname," from .nc file"
        nc_len = 1825 ! *** this is time dimension of the nc file (e.g., ncdump -h fdf.nc)
        allocate(mod_vals(1,1,10,nc_len)) ! give the dimensions
        allocate(nct(nc_len))
 
        call nc_dims(mod_file, mod_varname,dimnames,dimlens)
        call nc_read(mod_file, mod_varname,mod_vals)
        call nc_read(mod_file, "time", nct)
 
        ! time from model output (here .nc file) in second
        ! so need to convert to day
        nct = nct/86400
        !-----------------------------



        !--- MODEL SKILL SCORES: WSS_MAE
        !
        ! in observation, datenum takes 01-Jan-0000 as a reference point
        ! model started at 02-Jan-1990 (in model output .nc file, "time" started at 0)
        ! ==> need to convert to the same time reference
        !     also drop out first year of simulation as spin-up
        ! SO THEN time reference is 01-Jan-1991, OR at datenum 727199 (in case of observation)
        !                                        OR at date 364 (in case of model output)
        obs_datenum = obs_datenum - 727199 + 1
        nct = nct - 364 + 1
        new_len = nc_len - 364 ! to drop out the first year of simulation
        ! the model stop at 31-Dec-1994 (or "time" = 1824, in .nc file of model output,
        !                                   datenum 728659 in .dat file of observation) 
        ! select observation and model output in the same period of model output
        allocate(obs_dm_t(new_len), obs_dm_val(new_len))
        dm_pointer = 0
        do i = 1,size(obs_datenum)
                intdm = obs_datenum(i)
                if ((intdm .ge. 1) .and. (intdm .le. new_len)) then
                        dm_pointer = dm_pointer + 1
                        obs_dm_t(dm_pointer) = obs_datenum(i)
                        obs_dm_val(dm_pointer) = obs_vals(i)
                end if
        end do

        allocate(mod_dm_t(new_len),mod_dm_val(new_len))
        mod_dm_t = nct(365:nc_len)
        mod_dm_val = mod_vals(1,1,1,365:nc_len)

        ! find data available in both observation and model
        allocate(dm1(dm_pointer), dm2(dm_pointer), dm3(dm_pointer))
        k = 0
        do i = 1,dm_pointer !--- go through available observation
                do j = 1,new_len !--- go through available model output
                        if (obs_dm_t(i) .eq. mod_dm_t(j)) then
                                k = k + 1
                                dm1(k) = obs_dm_val(i) ! dm1 stores obsvations
                                dm2(k) = mod_dm_val(j) ! dm2 stores model outputs
                                dm3(k) = obs_dm_t(i)   ! dm3 stores common date
                        end if
                end do
        end do

        allocate(Oi(k), Mi(k), Oi_log(k), Mi_log(k))
        Oi = dm1(1:k)
        Mi = dm2(1:k)
        Oi_log = LOG10(Oi+1) ! use log transformation to reduce skewness of observations
        Mi_log = LOG10(Mi+1)

        ! mean obs
        Obar = sum(Oi)/k !or ((max(1,size(v_obs_vals)))
        Obar_log = sum(Oi_log)/k
        ! calculate wss
!           ! CASE: wss refined index
!        wss1 = sum(abs(Mi - Oi))
!        wss2 = c*sum(abs(Oi - Obar))
!        if (wss1 .le. wss2) then
!                wss = 1.0 - (wss1/wss2)
!        else
!                wss = (wss2/wss1) - 1.0
!        end if
           ! CASE: d1 (or MAE)
        wss1 = (1.0/k)*sum(abs(Mi-Oi))
        wss2 = (1.0/k)*sum(abs(Mi-Obar)+abs(Oi-Obar))
        wss1_log = (1.0/k)*sum(abs(Mi_log-Oi_log))
        wss2_log = (1.0/k)*sum(abs(Mi_log-Obar_log)+abs(Oi_log-Obar_log))
        wss = 0.5*(1.0 - (wss1/wss2))+0.5*(1-(wss1_log/wss2_log))


       if (mod_varname == 'ECO_no3') then
               fn_cost_func = -abs(1.3*wss) ! use weighting factor if needed
       else if (mod_varname == 'ECO_chl') then
              fn_cost_func = -abs(1.2*wss)
       else
             fn_cost_func = -abs(wss)
       end if 

       deallocate(dm1, dm2)
       deallocate(Oi, Mi, Oi_log, Mi_log)
       deallocate(obs_dm_t, obs_dm_val)
       deallocate(nct)
       deallocate(mod_vals)
       deallocate(obs_date, obs_datenum, obs_datenum_f, obs_vals)
       !---------------------------------

        return

end function
