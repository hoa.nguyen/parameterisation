subroutine sub_edit_fabmyaml(tp, tp_sval)

        implicit none

        integer, intent(in) :: tp
        real, intent(in) :: tp_sval

        real :: tp_tval
        real :: p_min, p_max

        character(len=10) :: val_txt
        character(len=200) :: wrt_yaml

        include 'tuned_p.f90'


        p_min = tp_minmax(tp,1)
        p_max = tp_minmax(tp,2)

        tp_tval = tp_sval*(p_max - p_min) + p_min

        write(val_txt, '(f10.7)') tp_tval

        wrt_yaml = './wrt_new_fabmyaml.sh   '//tp_names(tp)//val_txt
        call execute_command_line(wrt_yaml)


end subroutine sub_edit_fabmyaml
