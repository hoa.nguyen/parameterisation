#!/bin/bash

# or get parameter names and values that will be updated in fabm.yaml
tp_name=$1
tp_val=$2

# write corresponding values to fabm.yaml
sed -i 's/'$tp_name'[[:space:]]*:.*#/'$tp_name' : '$tp_val' #/' fabm.yaml
