! Hoa Nguyen. May 24th 2023

program call_bgc_model
      !
      use ncio

      ! precision format
      integer, parameter :: r3 = selected_real_kind(3)


      !--- path to executable file of a biogeochemical (BGC) model
      character(len=100), parameter :: bgc_exe = "/work/gg0877/g260193/models_development/FABM_GOTM_EwE//gotm_built/gotm"


      !--- pso variables and parameters
      integer, parameter :: n_validated_var = 4 ! validated variables (Dia, Flag, NO3, PO4, SiO)
      integer, parameter :: n_tp = 4 ! number of tuning parameters (check tuned_p.f90 for the value)
      integer, parameter :: psize = 8 ! swarm size (or number of scenarios/runs at the same iteration/time) 
      integer, parameter :: dimen = n_tp ! particle dimension = number of tuning parameters 
      integer, parameter :: max_iter = 10 ! iteration -- how many time to run the PSO (stoping condition) [270]
      integer, parameter :: dis_iter(2)= (/3,6/) ! iteration/step at which particle/scenario is reset
                                       ! to get away from the current position/parameters
                                       ! to ensure that it will not be trapped in 1 place
      integer, parameter :: xmin = 0, xmax = 1 ! parameter rescaled in [0 1] to ensure normal distribution
                                               ! when randomly assign values
      real, parameter :: vmin = -0.5, vmax = 0.5 ! vmax = 0.5*(xmax - xmin), vmin = -vmax
                                                 ! speed limit that a particle/scenario move from its own position/values
         ! constriction coefficients
      integer, parameter :: kappa = 1, wdamp = 1 ! wdamp: damping ratio of Inertia Coefficient (PSO alg. parameters)
      real, parameter :: phi1 = 2.05, phi2 = 2.05 ! PSO alg. parameters (ref. Poli 2007)
      real :: phi, chi                            ! PSO alg. parameters
      real :: w, c1, c2                           ! PSO alg. parameters
      real,parameter :: ob_damp = 0.8             ! to ensure the velocity reduce after each iteration

         ! varialbes to hold swarm and particles information
      real(KIND=r3) :: x(psize, dimen), v(psize, dimen), x_init(dimen)
      real(KIND=r3) :: icost(psize), icost_var(psize, n_validated_var)
      real(KIND=r3) :: x_best(psize, dimen), icost_best(psize)

      real(KIND=r3) :: x_it(max_iter,psize,dimen), v_it(max_iter,psize,dimen)
      real(KIND=r3) :: cost_var_it(max_iter,psize,n_validated_var)
      real(KIND=r3) :: cost_it(max_iter,psize)

      real(KIND=r3) :: pbest_x_it(max_iter,psize,dimen)
      real(KIND=r3) :: pbest_cost_it(max_iter,psize)

      real(KIND=r3) :: gbest_cost, gbest_x(dimen)
      real(KIND=r3) :: gbest_x_it(max_iter,dimen)
      real(KIND=r3) :: gbest_cost_it(max_iter)
      real(KIND=r3) :: gbest_vars_cost_it(n_validated_var+1)

      integer :: pi, tp, vv, it, dit, tt ! loop

      real(KIND=r3) :: xc1(dimen), xc2(dimen)
      real(KIND=r3) :: xupdate(dimen), xrand(dimen)
      logical :: IsOutLb, IsOutUb


      !--- varialbes for cost function inputs
      character(len=*), parameter :: obs_file(4)=(/'validation/Syltobs_Chl_val.dat', &
                                                   'validation/Syltobs_NO3_val.dat', &
                                                   'validation/Syltobs_PO4_val.dat', &
                                                   'validation/Syltobs_SiO_val.dat'/)
      character(len=*), parameter :: mod_file = 'sylt.nc'
      character(len=*), parameter :: mod_vars(4)=(/'ECO_chl', 'ECO_no3', &
                                                        'ECO_pho', 'ECO_sil'/)

      !--- format to write to file
      character(len=30) :: fmt10, fmt11, fmt14, fmt15


!==============================================================================

      !>>> CALCULATE PSO CONSTRICTION COEFFICIENTS <<<!
      phi = phi1 + phi2
      chi = 2*kappa/abs(2-phi-sqrt(phi*phi-4*phi))
      w = chi
      c1 = chi*phi1
      c2 = chi*phi2
      !-----------------------------------------------!


      !>>> RANDOMLY INITIALIZE PARTICLE POSITIONS <<<!
      !CALL RANDOM_SEED
         !--- edit fabm.yaml, call bgc model and evaluate model fitness
      do pi =1,psize ! go through each particle of the swarm
         icost(pi) = -1.0
         icost(pi) = sqrt(icost(pi)) ! first assign NaN value
         do while (icost(pi)/=icost(pi)) ! repeat as long as the particle cost is NaN
              CALL RANDOM_SEED
              CALL RANDOM_NUMBER(x_init) ! particle positions/scenario's parameters in scale of [0 1]
              do tp = 1,n_tp ! for each tuning parameter
                 call sub_edit_fabmyaml(tp, x_init(tp)) ! update its value in fabm.yaml
              end do
              ! then run the bgc model
              call execute_command_line(bgc_exe)
              ! then evaluate the model fitness/cost
              do vv = 1,n_validated_var    ! for each validated varialbe
                 icost_var(pi,vv)=fn_cost_func(obs_file(vv),mod_file,mod_vars(vv))
              end do
              icost(pi) = sum(icost_var(pi,:))
         end do
         x(pi,:) = x_init
      end do
      icost_best = icost ! after the first INTIALIZATION, particle cost is also its best cost.
      x_best = x         ! and the current position is also the best position
      
      !--------------------------------------------------------------------------------------

      !>>> Initialize Global best value (is minimum of all particle costs/fitness)<<<!
      gbest_cost = minval(icost_best)
      do pi=1,psize
              if (icost_best(pi)==gbest_cost) then
                      gbest_x=x_best(pi,:) ! global best position after first initialization
                      gbest_vars_cost_it=(/gbest_cost,icost_var(pi,:)/) ! added 16May23
              end if
      end do

      ! clear reused variables in pso
      icost = 0.0
      icost_var = 0.0
      !---------------------------------------------------------------------------------------
      
      !>>> MAIN PSO LOOP <<<
      WRITE(fmt10, '(A,I2,A)') '(', n_validated_var, '(1X,F7.3))'
      OPEN(UNIT=10, FILE='validated_var_cost_it.txt', ACTION="write", STATUS="replace")

      WRITE(fmt11, '(A,I2,A)') '(', dimen, '(1X,F7.3))'
      OPEN(UNIT=11, FILE='particle_v_it.txt', ACTION="write", STATUS="replace")

      OPEN(UNIT=12, FILE='particle_bestpos_it.txt', ACTION="write", STATUS="replace")

      OPEN(UNIT=13, FILE='g_bestpos_it.txt', ACTION="write", STATUS="replace")

      WRITE(fmt14, '(A)') '(F7.3)'
      OPEN(UNIT=14, FILE='g_bestcost_it.txt', ACTION="write", STATUS="replace")

      WRITE(fmt15, '(A,I2,A)') '(', n_validated_var+1, '(1X,F7.3))'
      OPEN(UNIT=15, FILE='g_vars_bcost_it.txt', ACTION="write", STATUS="replace")


      do it=1,max_iter
               CALL RANDOM_SEED
               !--- check if the current iteration is disturb iteration
               do dit=1,size(dis_iter)
                 if (it == dis_iter(dit)) then
                       !--- disturb velocity of all particles
                       call random_number(v)
                  end if
               end do
               !--- loop through each particle

              do pi=1,psize
                 icost(pi) = -1.0
                 icost(pi) = sqrt(icost(pi)) ! to sure run is not NaN
                 do while (icost(pi)/=icost(pi))
                      !--- update Velocity
                      call random_number(xc1)
                      call random_number(xc2)
                      v(pi,:)=w*(v(pi,:)+c1*xc1*(x_best(pi,:)-x(pi,:))+c2*xc2*(gbest_x-x(pi,:)))
                      !--- apply velocity limits and update velocity
                      v(pi,:) = max(v(pi,:), vmin)
                      v(pi,:) = min(v(pi,:), vmax)

                      !--- Update Position
                      x(pi,:) = x(pi,:) + v(pi,:)

                      !--- Velocity and Position Effect when out of bounds:
                      call random_number(xrand)
                      do tp = 1,n_tp
                      !       when new position is out of lower bound
                              if (x(pi,tp) .lt. xmin) then
                                      x(pi,tp) = xrand(tp)*ob_damp*x(pi,tp)
                                      v(pi,tp) = (xrand(tp)*ob_damp-1)*v(pi,tp)
                              end if
                      !       when new position is out of upper bound
                              if (x(pi,tp) .gt. xmax) then
                                      x(pi,tp) = 1 - xrand(tp)*ob_damp*(x(pi,tp)-1)
                                      v(pi,tp) = (xrand(tp)*ob_damp-1)*v(pi,tp)
                              end if
                      end do
                     !--- Evaluation
                      !    update new prameters
                      do tp = 1,n_tp
                         call sub_edit_fabmyaml(tp,x(pi,tp))
                      end do
                      call execute_command_line(bgc_exe)
                      do vv = 1,n_validated_var
                         icost_var(pi,vv)=fn_cost_func(obs_file(vv),mod_file,mod_vars(vv))

                      end do
                      icost(pi) = sum(icost_var(pi,:))
                 end do
                 WRITE(10, FMT=fmt10) (icost_var(pi,tt), tt=1,n_validated_var)
                 WRITE(11, FMT=fmt11) (v(pi,tt), tt=1,dimen)
              end do !======= psize =========
              cost_var_it(it,:,:)=icost_var


              ! Update Particle (or Personal) and Global Best
              do pi = 1,psize
                 if (icost(pi) .lt. icost_best(pi)) then
                         icost_best(pi)=icost(pi) ! update personal best cost/fitness value
                         x_best(pi,:) = x(pi,:)   ! update personal best position/parameters
                         if (icost_best(pi) .lt. gbest_cost) then
                                 gbest_cost = icost_best(pi) ! update global best value
                                 gbest_x = x_best(pi,:)      ! update global best position
                                 gbest_vars_cost_it = (/gbest_cost, icost_var(pi,:)/) ! added 16May23
                         end if
                 end if
                 WRITE(12, FMT=fmt11) (x_best(pi,tt),tt=1,dimen)
              end do
              WRITE(13, FMT=fmt11) (gbest_x(tt), tt=1,dimen)
              WRITE(14, FMT=fmt14) (gbest_cost)
              WRITE(15, FMT=fmt15) (gbest_vars_cost_it(tt),tt=1,n_validated_var+1)

              x_it(it,:,:) = x
              v_it(it,:,:) = v
              cost_it(it,:) = icost

              pbest_x_it(it,:,:) = x_best
              pbest_cost_it(it,:) = icost_best

              gbest_x_it(it,:) = gbest_x
              gbest_cost_it(it) = gbest_cost

              ! clear reused variables in pso
              icost = 0.0
              icost_var = 0.0

      end do !====== max_iter======
      CLOSE(UNIT=10)
      CLOSE(UNIT=11)
      CLOSE(UNIT=12)
      CLOSE(UNIT=13)
      CLOSE(UNIT=14)
      CLOSE(UNIT=15)
    

!==============================================================================

      contains
              include 'sub_edit_fabmyaml.f90'
              include 'fn_cost_func.f90'

end program call_bgc_model
