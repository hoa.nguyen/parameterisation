# Parameterisation by Particle Swarm Optimizer (PSO)

The PSO (based on Poli et al. 2007) is written in Fortran90 to parameterize biogeochemical models (here ECOSMO (Daewel and Schrum, 2013; Schrum et al., 2006a)) in FABM (Framework for Aquatic Biogeochemical Models, Bruggeman and Bolding 2014). 

Author: Hoa Nguyen.<br>
Institution: Helmholtz Zentrum Hereon.<br>
&emsp;&emsp;&emsp;&emsp;System Analysis and Modeling (KS)<br>
&emsp;&emsp;&emsp;&emsp;Matter Transport and Ecosystem Dynamics (KST)<br>
&emsp;&emsp;&emsp;&emsp;Email: hoa.nguyen@hereon.de / ngthaihoa@yahoo.com<br>

## The Latest Version

Details of the latest version can be found on the GitLab project page under <https://codebase.helmholtz.cloud/hoa.nguyen/parameterisation>.

## Program structure
**PSO**<br> 
&#124;<br> 
&#124;---> tuned_p.f90<br>
&#124;---> call_bgc_model.f90<br>
&#124;&emsp;&emsp;&#124;---> sub_edit_fabmyaml.f90<br>
&#124;&emsp;&emsp;&#124;&emsp;&emsp;&#124;--->wrt_new_fabmyaml.sh<br>
&#124;&emsp;&emsp;&#124;---> execute bgc model (call execute_command_line(bgc_exe))<br>
&#124;&emsp;&emsp;&#124;---> fn_cost_func.f90<br>


**Extract parameters at given PSO iterations and run a bgc model**<br>
&#124;<br> 
&#124;---> get_tp_names.f90<br>
&#124;---> run_bgc_at_it.sh<br>
&#124;&emsp;&emsp;&#124;---> exp_it_gbestpos.f90<br>
&#124;&emsp;&emsp;&#124;&emsp;&emsp;&#124;---> sub_edit_fabmyaml.f90<br>
&#124;&emsp;&emsp;&#124;&emsp;&emsp;&#124;&emsp;&emsp;&#124;--->wrt_new_fabmyaml.sh<br>
&#124;&emsp;&emsp;&#124;---> call_bgc_model_short.f90<br><br>

## Usage 
**Adjustment might need to be made to adapt the program for other applications/cases**<br>
The instruction is tested in Linux OS and Levante system <br>

1. ***tuned_p.f90***: declare parameters that need/want to be tuned and their corresponding ranges. This file contains most of parameters in ECOSMO model.<br> 
2. ***call_bgc_model.f90***: might need to adjust, also see comments in the file for more details<br>
&emsp;&emsp; line 12: bgc_exe: the path to a bgc model executable file, which is compiled with FABM<br>
&emsp;&emsp; line 16: n_validated_var<br>
&emsp;&emsp; line 17: n_tp: this must be the same number as declare in tuned_p.f90<br>
&emsp;&emsp; line 20: max_iter: number of times when to run the pso<br>
&emsp;&emsp; line 21: dis_iter: how many and at which iteration the pso should be reset to avoid being trapped in a local minima/maxima<br>
&emsp;&emsp; line 60: obs_file: observations<br>
&emsp;&emsp; line 64: mod_file: model output file<br>
&emsp;&emsp; line 65: mod_vars: number must be same as in line 16, provide variable names as in the model output<br>
3. ***fn_cost_func.f90***: adjust the way to read in observation and model output depend on data format and length. And adjust the cost function as needed. The cost function in this case is Wilmot Skill Score (Willmott et al., 2012). Please see comments in the file for more details.<br>
4. ***Makefile***: move to PSO folder and from command line run the Makefile after making adjustment as needed by given case/application to create executable file ***bgc_pso_exe.out***. Then run this executable file to parameterize the model with PSO if the run time is short. In case run time is long, then use ****sbatch.pbs***
5. ***View PSO output***: use scripts provided in view_pso_output.ipynb (Matlab in Jupyter notebook) or view_pso_output.pdf
6. ***Extract model paremeters at given PSO iterations and run a bgc model***: <br>
&emsp;&emsp; complile and run ***get_tp_names.f90*** to extract tuned parameters. <br>
&emsp;&emsp; change "bgc_exe" in ***call_bgc_model_short.f90*** as needed (this "bgc_exe" should be the same as in call_bgc_model.f90). <br>
&emsp;&emsp; in file ***run_bgc_at_it.sh***, line 7: change number "26" if the line number is not correct; line 17: change file name (eg. sylt*) as needed. Then run the .sh file with a list of iteration at which parameters will be extracted (eg., ./run_bgc_at_it.sh 3 5) <br>

## Support
Please get in touch via hoa.nguyen@hereon.de or ngthaihoa@yahoo.com if there is further questions, suggesstions, etc. 

## Authors and acknowledgment
This PSO is a part of CHANCES project funded by I2B-Funds.

## License
This parameterisation program is free to use: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation (https://www.gnu.org/licenses/gpl.html), either version 3 of the License, or (at your option) any later version.
It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
Copyright 2023 Hoa Nguyen (hoa.nguyen@hereon.de and ngthaihoa@yahoo.com)

## Project status
This program is still in development and improvement.
